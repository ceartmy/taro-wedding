# wedding

> 体验二维码

<p align=center>
    <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/QRcode.jpg" width="200" />
</p>

## 站点演示

| <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/index_01.jpg" width="200"/> | <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/index_02.jpg" width="200"/> | <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/index_03.jpg" width="200" /> | <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/index_04.jpg" width="200" /> | <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/index_05.jpg" width="200"/> | <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/index_06.jpg" width="200"/> | <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/index_07.jpg" width="200" /> | <img src="https://gitee.com/ceartmy/taro-wedding/raw/master/other/index_08.jpg" width="200" /> |

## 功能实现
- [x] 代码打字效果
- [x] 图片切换效果
- [x] 评论留言
- [x] 出席申请
- [x] 生成海报
- [x] 地图导航
- [x] 图片管理
- [ ] 订阅功能
